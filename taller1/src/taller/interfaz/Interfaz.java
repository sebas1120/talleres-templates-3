package taller.interfaz;

import java.util.ArrayList;
import java.util.Scanner;

import taller.mundo.Jugador;
import taller.mundo.LineaCuatro;

public class Interfaz 
{
	/**
	 * Juego actual
	 */
	private LineaCuatro juego;

	/**
	 * Escáner de texto ingresado por el usuario en la consola
	 */
	private Scanner sc;

	/**
	 * Crea una nueva instancia de la clase de interacción del usuario con la consola
	 */
	public Interfaz()
	{
		sc= new Scanner (System.in);
		while (true)
		{
			imprimirMenu();
			try
			{
				int opt=Integer.parseInt(sc.next());
				if(opt==1)
				{
					empezarJuego();
				}
				else if(opt==2)
				{
					empezarJuegoMaquina();
				}
				else if(opt==3)
				{
					System.out.println("¡Vuelva pronto!");
					break;
				}
				else
				{
					System.out.println("Comando inválido");
					continue;
				}
			}
			catch (NumberFormatException e)
			{
				System.out.println("Comando inválido");
				continue;
			}


		}
	}
	/**
	 * Imprime el menú principal
	 */
	public void imprimirMenu()
	{
		System.out.println("------------------------LÍNEA CUATRO------------------------");
		System.out.println("-Menú principal-");
		System.out.println("Ingrese un comando:");
		System.out.println("1. Empezar juego con amigos");
		System.out.println("2. Empezar juego vs máquina");
		System.out.println("3. Salir");		
	}

	/**
	 * Crea un nuevo juego
	 */
	public void empezarJuego()
	{
		//TODO
		try 
		{
			Scanner sc = new Scanner(System.in);
			System.out.println("Ingrese la cantidad de jugadores a jugar: ");
			int cantJug = sc.nextInt();
			ArrayList<Jugador> listaJugadores = new ArrayList<>();
			int i = 0;
			while(i<cantJug)
			{
				System.out.println("Jugador " + (i+1) + " ingrese su nombre completo: ");
				String nombre1 = sc.next();
				System.out.println("Ingrese su simbolo: ");
				String simbolo = sc.next();
				Jugador jg = new Jugador(nombre1, simbolo);
				listaJugadores.add(jg);
				i++;
			}
			System.out.println("Ingrese el numero de filas: ");
			int filas = sc.nextInt();
			System.out.println("Ingrese el numero de columnas: ");
			int columnas = sc.nextInt();
			if(filas<=3 || columnas<=3)
			{
				juego.fin();
			}
			else
			{
				juego = new LineaCuatro(listaJugadores, filas, columnas);
				imprimirTablero(filas, columnas);
				System.out.println();
				juego(filas, columnas);
			}
		}
		catch (Exception e) 
		{
			// TODO: handle exception
		}
	}

	/**
	 * Modera el juego entre jugadores
	 */
	public void juego(int fil, int col)
	{
		Scanner sc = new Scanner(System.in);

		while(juego.fin()==false)
		{
			System.out.println(juego.darAtacante()+" es su turno");
			System.out.println("Seleccione una columna");
			int colum = sc.nextInt()-1;
			juego.registrarJugada(colum);
			imprimirTablero(fil, col);
			juego.terminar();
			System.out.println();
		}

		System.out.println("Ha ganado el jugador " + juego.darAtacante());
		System.out.println("Si desea reiniciar el juego ponga un 1 de lo contrario de 0");
		int reinicio = sc.nextInt();
		if(reinicio==1)
		{
			empezarJuegoMaquina();
		}
		else
		{
			juego.fin();
		}
	}
	/**
	 * Empieza el juego contra la máquina
	 */
	public void empezarJuegoMaquina()
	{
		try 
		{
			Scanner sc = new Scanner(System.in);
			ArrayList<Jugador> listaJugadores = new ArrayList<>();
			System.out.println("Jugador ingrese su nombre completo: ");
			String nombre1 = sc.next();
			System.out.println("Ingrese su simbolo: ");
			String simbolo = sc.next();
			Jugador jg = new Jugador(nombre1, simbolo);
			Jugador maquina = new Jugador("maquina", "m");
			listaJugadores.add(jg);
			listaJugadores.add(maquina);
			System.out.println("Ingrese el numero de filas: ");
			int filas = sc.nextInt();
			System.out.println("Ingrese el numero de columnas: ");
			int columnas = sc.nextInt();			
			if(filas<=3 || columnas<=3)
			{
				juego.fin();
			}
			else
			{
				juego = new LineaCuatro(listaJugadores, filas, columnas);
				imprimirTablero(filas, columnas);
				System.out.println();
				juegoMaquina(filas,columnas);
			}
		}
		catch (Exception e) 
		{
			// TODO: handle exception
		}
	}
	/**
	 * Modera el juego contra la máquina
	 */
	public void juegoMaquina(int fil, int col)
	{
		Scanner sc = new Scanner(System.in);

		while(juego.fin()==false)
		{
			System.out.println(juego.darAtacante()+" es su turno");
			System.out.println("Seleccione una columna");
			int colum = sc.nextInt()-1;
			juego.registrarJugada(colum);
			juego.registrarJugadaAleatoria();
			imprimirTablero(fil, col);
			juego.terminar();
			System.out.println();
		}

		System.out.println("Ha ganado el jugador " + juego.darAtacante());
		System.out.println("Si desea reiniciar el juego ponga un 1 de lo contrario de 0");
		int reinicio = sc.nextInt();
		if(reinicio==1)
		{
			empezarJuegoMaquina();
		}
		else
		{
			juego.fin();
		}
	}

	/**
	 * Imprime el estado actual del juego
	 */
	public void imprimirTablero(int fil, int col)
	{
		for (int i = 0; i < fil; i++) 
		{
			System.out.println();
			for (int j = 0; j < col; j++) 
			{
				System.out.print(juego.darTablero()[i][j]);
			}

		}

	}
}
