package taller.mundo;

import java.util.ArrayList;
import java.util.Random;

public class LineaCuatro 
{
	/**
	 * Matriz que representa el estado actual del juego.
	 */
	private String[][] tablero;

	/**
	 * Lista que contiene a los jugadores
	 */
	private ArrayList<Jugador> jugadores;

	/**
	 * Nombre del jugador en turno
	 */
	private String atacante;

	/**
	 * Determina si el juego se terminó
	 */
	private boolean finJuego;

	/**
	 * El número del jugador en turno
	 */
	private int turno;

	/**
	 * Crea una nueva instancia del juego
	 */
	public LineaCuatro(ArrayList <Jugador> pJugadores,int pFil, int pCol)
	{
		tablero = new String[pFil][pCol];
		for(int i=0;i<tablero.length;i++)
		{
			for(int j=0;j<tablero[0].length;j++)
			{
				tablero[i][j]="_0_";
			}

		}
		jugadores = pJugadores;
		finJuego = false;
		turno = 0;
		atacante = jugadores.get(turno).darNombre();
	}
	/**
	 * Retorna el tablero del juego
	 * @return tablero
	 */
	public String[][] darTablero()
	{
		return tablero;
	}

	/**
	 * Retorna el jugador en turno
	 * @return atacante jugador en turno
	 */
	public String darAtacante()
	{
		return atacante;
	}
	/**
	 * Determina si el juego termina
	 * @return true si el juego se termino, false de lo contrario
	 */
	public boolean fin()
	{
		return finJuego;
	}

	/**
	 * Registra una jugada aleatoria
	 */
	public void registrarJugadaAleatoria()
	{
		boolean centinela = false;
		Random rnd = new Random();
		int cantidadFilas = tablero.length;
		int cantidadColumnas = tablero[0].length;
		int numAle = (int)(rnd.nextDouble()*cantidadColumnas+1);

		for (int i = cantidadFilas-1; i != 0 && !centinela; i--) 
		{
			if(tablero[i][numAle].equals("_0_"))
			{
				tablero[i][numAle]=("_" + jugadores.get(turno).darSimbolo() + "_");

				centinela = true;
			}

		}
		if(centinela==true)
		{
			turno++;
			if(turno > jugadores.size()-1)
			{
				turno=0;
			}
			atacante = jugadores.get(turno).darNombre();
			
		}
		


	}



	/**
	 * Registra una jugada en el tablero
	 * @return true si la jugada se pudo realizar, false de lo contrario
	 */
	public boolean registrarJugada(int col)
	{
		//TODO
		try 
		{
			int cantidadFilas = tablero.length;
			boolean centinela = false;
			for (int i = cantidadFilas-1; i != 0 && !centinela; i--) 
			{
				if(tablero[i][col].equals("_0_"))
				{
					tablero[i][col]=("_" + jugadores.get(turno).darSimbolo() + "_");

					centinela = true;
				}

			}
			if(centinela==true)
			{
				turno++;
				if(turno > jugadores.size()-1)
				{
					turno=0;
				}
				atacante = jugadores.get(turno).darNombre();
				return true;
			}
			else
			{
				return false;
			}
		}
		catch (Exception e) 
		{
			return false;
		}


	}

	/**
	 * Determina si una jugada causa el fin del juego
	 * @return true si la jugada termina el juego false de lo contrario
	 */
	public void terminar()
	{

		boolean gano = false;
		for (int i = 0; i < tablero.length && !gano; i++) 
		{
			int contador = 1;
			for (int j = 0; j < tablero[i].length-1 && !gano; j++) 
			{
				if(!(tablero[i][j].equals("_0_")) && tablero[i][j].equals(tablero[i][j+1]))
				{
					contador++;
					if(contador==4)
					{
						gano = true;
					}
				}
				else
				{
					contador=1;
				}
			}
		}

		for (int i = 0; i < tablero.length-3 && !gano; i++) 
		{
			for (int j = 0; j < tablero[i].length && !gano; j++) 
			{
				if(!(tablero[i][j].equals("_0_")) && tablero[i][j].equals(tablero[i+1][j]))
				{
					if(tablero[i][j].equals(tablero[i+2][j]))
					{
						if(tablero[i][j].equals(tablero[i+3][j]))
						{							
							gano = true;
						}
					}
				}
			}
		}

		for (int i = 0; i < tablero.length-3 && !gano; i++) 
		{
			for (int j = 0; j < tablero[i].length-3 && !gano; j++) 
			{
				if(!(tablero[i][j].equals("_0_")) && tablero[i][j].equals(tablero[i+1][j+1]))
				{
					if(tablero[i][j].equals(tablero[i+2][j+2]))
					{
						if(tablero[i][j].equals(tablero[i+3][j+3]))
						{							
							gano = true;
						}
					}
				}
			}
		}

		for (int i = 3; i < tablero.length && !gano; i++) 
		{
			for (int j = 0; j < tablero[i].length-3 && !gano; j++) 
			{
				if(!(tablero[i][j].equals("_0_")) && tablero[i][j].equals(tablero[i-1][j+1]))
				{
					if(tablero[i][j].equals(tablero[i-2][j+2]))
					{
						if(tablero[i][j].equals(tablero[i-3][j+3]))
						{							
							gano = true;
						}
					}
				}
			}
		}
		if(gano==true)
		{
			finJuego=true;
			turno++;
			if(turno > jugadores.size()-1)
			{
				turno=0;
			}
			atacante = jugadores.get(turno).darNombre();
		}

	}



}
